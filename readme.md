# C#からOpenCVを触る
- [C#でOpenCVを使う（C++/CLIによる方法）](http://whoopsidaisies.hatenablog.com/entry/2014/01/12/113050)
- [OpenCVをC++/CLIでラップ - 何でもプログラミング](http://any-programming.hatenablog.com/entry/2017/03/03/210419)

# 環境
- visual studio 2015
- opencv 3.2
- 上記のURL見ながら設定を色々と

# やりたかったこと
- C++だけだとフォーム作るの難しい!!
- C#だけだとOpenCVに若干不安がある!!
- C++/CLIでOpenCVをラップしてしまって、ライブラリとしてC#から使ってしまえ!!(言葉が正しいか怪しい)

# 残っている作業
- 明るさ変化等のバリエーション



# 参考にしたい
- https://detail.chiebukuro.yahoo.co.jp/qa/question_detail/q11129950331