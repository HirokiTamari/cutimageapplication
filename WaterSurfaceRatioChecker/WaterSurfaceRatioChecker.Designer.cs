﻿namespace WaterSurfaceRatioChecker {
    partial class waterSurfaceRatioCheckerForm {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent() {
            this.selectCsvFileButton = new System.Windows.Forms.Button();
            this.csvFileNameTextBox = new System.Windows.Forms.TextBox();
            this.startButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // selectCsvFileButton
            // 
            this.selectCsvFileButton.Location = new System.Drawing.Point(21, 12);
            this.selectCsvFileButton.Name = "selectCsvFileButton";
            this.selectCsvFileButton.Size = new System.Drawing.Size(75, 23);
            this.selectCsvFileButton.TabIndex = 0;
            this.selectCsvFileButton.Text = "csv選択";
            this.selectCsvFileButton.UseVisualStyleBackColor = true;
            this.selectCsvFileButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.selectCsvFileButton_MouseClick);
            // 
            // csvFileNameTextBox
            // 
            this.csvFileNameTextBox.Location = new System.Drawing.Point(111, 14);
            this.csvFileNameTextBox.Name = "csvFileNameTextBox";
            this.csvFileNameTextBox.Size = new System.Drawing.Size(328, 19);
            this.csvFileNameTextBox.TabIndex = 1;
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(364, 51);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(75, 23);
            this.startButton.TabIndex = 2;
            this.startButton.Text = "スタート";
            this.startButton.UseVisualStyleBackColor = true;
            this.startButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.startButton_MouseClick);
            // 
            // waterSurfaceRatioCheckerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 91);
            this.Controls.Add(this.startButton);
            this.Controls.Add(this.csvFileNameTextBox);
            this.Controls.Add(this.selectCsvFileButton);
            this.Name = "waterSurfaceRatioCheckerForm";
            this.Text = "水面チェッカー";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button selectCsvFileButton;
        private System.Windows.Forms.TextBox csvFileNameTextBox;
        private System.Windows.Forms.Button startButton;
    }
}

