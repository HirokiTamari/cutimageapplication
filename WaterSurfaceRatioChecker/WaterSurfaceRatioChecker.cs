﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using OpenCVLibrary;

namespace WaterSurfaceRatioChecker {
    public partial class waterSurfaceRatioCheckerForm : Form {
        string csvFileName;
        string saveDirectoryName;

        public waterSurfaceRatioCheckerForm() {
            InitializeComponent();
        }

        private void selectCsvFileButton_MouseClick(object sender, MouseEventArgs e) {
            //現状csvファイルにだけ対応
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "csvファイル(*.csv)|*.csv";
            ofd.InitialDirectory = System.IO.Directory.GetCurrentDirectory();

            if (ofd.ShowDialog() == DialogResult.OK) {
                //テキストボックスに選択された画像の名前を表示
                this.csvFileNameTextBox.Text = ofd.FileName;
                this.csvFileName = ofd.FileName;
                this.saveDirectoryName = System.IO.Path.GetDirectoryName(ofd.FileName) + "\\" +
                                         System.IO.Path.GetFileNameWithoutExtension(ofd.FileName) + "\\";
            }
        }

        private void startButton_MouseClick(object sender, MouseEventArgs e) {
            System.IO.StreamReader file = new System.IO.StreamReader(this.csvFileName);

            if (!System.IO.Directory.Exists(saveDirectoryName)) {
                System.IO.Directory.CreateDirectory(saveDirectoryName);
            }

            string line;
            double waterSurfaceRatio;
            string readFileName;
            string saveFileName;
            while ((line = file.ReadLine()) != null) {
                string[] tmp = line.Split(',');
                readFileName = System.IO.Path.GetDirectoryName(this.csvFileName).ToString() + "\\" + tmp[0];
                saveFileName = this.saveDirectoryName + tmp[0];
                waterSurfaceRatio = Convert.ToDouble(tmp[1]);
                OpenCVLibrary.ImageProcesser.waterSurfaceDraw(readFileName, saveFileName, waterSurfaceRatio);
            }
        }
    }
}
