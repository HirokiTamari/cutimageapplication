// これは メイン DLL ファイルです。

#include "stdafx.h"

#include "OpenCVLibrary.h"
#include <string>

/// <summary>
/// プログラムが配置してあるディレクトリ内にあるcolor.jpgと
/// そのグレースケース画像を表示する関数
/// </summary>
void OpenCVLibrary::ImageProcesser::showOriginalAndGrayScaleImage() {
	//画像読み込み
	cv::Mat image = cv::imread("color.jpg");

	//GrayScale変換
	cv::Mat image_gray;
	cv::cvtColor(image, image_gray, cv::COLOR_BGR2GRAY);

	//表示
	cv::imshow("image", image);
	cv::imshow("image_gray", image_gray);

}

/// <summary>
/// ファイルを受け取ってグレースケールで表示する関数
/// </summary>
/// <param name="filename">ファイルの名前(パス)を渡す</param>
void OpenCVLibrary::ImageProcesser::showGrayScaleImage(System::String ^filename) {
	std::string stringFileName = msclr::interop::marshal_as<std::string>(filename);

	//画像読み込み
	cv::Mat image = cv::imread(stringFileName);

	//GrayScale変換
	cv::Mat image_gray;
	cv::cvtColor(image, image_gray, cv::COLOR_BGR2GRAY);

	//表示
	cv::imshow("image_gray", image_gray);
}

//void OpenCVLibrary::ImageProcesser::saveCutImage(System::String ^filename, int x, int y, int width, int height) {
//	std::string stringFileName = msclr::interop::marshal_as<std::string>(filename);
//
//	std::cout << "x : " << x << " y : " << y << " width : " << width << " height : " << height << std::endl;
//}

/// <summary>
/// 画像のある矩形部分を切り抜いて保存する関数(カラー)
/// 切り抜いた後のりサイズも可能
/// </summary>
/// <param name="readImageFileName">画像ファイルの名前(パス)</param>
/// <param name="saveImageFileName">画像ファイルの保存名(パス)</param>
/// <param name="x">切り抜き矩形部分左上の基準点X座標</param>
/// <param name="y">切り抜き矩形部分左上の基準点Y座標</param>
/// <param name="width">切り抜き矩形部分の幅</param>
/// <param name="height">切り抜き矩形部分の高さ</param>
/// <param name="outputWidth">吐き出し画像サイズの幅</param>
/// <param name="outputHeight">吐き出し画像サイズの高さ</param>
void OpenCVLibrary::ImageProcesser::saveCutImage(System::String ^readImageFileName, System::String ^saveImageFileName, int x, int y, int width, int height, int outputWidth, int outputHeight) {
	std::string stringReadImageFileName = msclr::interop::marshal_as<std::string>(readImageFileName);
	std::string stringSaveImageFileName = msclr::interop::marshal_as<std::string>(saveImageFileName);
	cv::Mat originalImage = cv::imread(stringReadImageFileName);
	cv::Rect rect(x, y, width, height);
	cv::Mat result = cv::Mat(originalImage, rect).clone();
	cv::resize(result, result, cv::Size(), (double)outputWidth/(double)result.cols, (double)outputHeight/(double)result.rows);
	cv::imwrite(stringSaveImageFileName, result);
}

/// <summary>
/// 画像のある矩形部分を切り抜いて保存する関数(グレースケール)
/// 切り抜いた後のりサイズも可能
/// </summary>
/// <param name="readImageFileName">画像ファイルの名前(パス)</param>
/// <param name="saveImageFileName">画像ファイルの保存名(パス)</param>
/// <param name="x">切り抜き矩形部分左上の基準点X座標</param>
/// <param name="y">切り抜き矩形部分左上の基準点Y座標</param>
/// <param name="width">切り抜き矩形部分の幅</param>
/// <param name="height">切り抜き矩形部分の高さ</param>
/// <param name="outputWidth">吐き出し画像サイズの幅</param>
/// <param name="outputHeight">吐き出し画像サイズの高さ</param>
void OpenCVLibrary::ImageProcesser::saveCutGrayScaleImage(System::String ^readImageFileName, System::String ^saveImageFileName, int x, int y, int width, int height, int outputWidth, int outputHeight) {
	std::string stringReadImageFileName = msclr::interop::marshal_as<std::string>(readImageFileName);
	std::string stringSaveImageFileName = msclr::interop::marshal_as<std::string>(saveImageFileName);
	cv::Mat originalImage = cv::imread(stringReadImageFileName, cv::IMREAD_GRAYSCALE);
	cv::Rect rect(x, y, width, height);
	cv::Mat result = cv::Mat(originalImage, rect).clone();
	cv::resize(result, result, cv::Size(), (double)outputWidth / (double)result.cols, (double)outputHeight / (double)result.rows);
	cv::imwrite(stringSaveImageFileName, result);

}

/// <summary>
/// 画像のある矩形部分を切り抜いて保存する関数(HSVのVを半減)
/// 切り抜いた後のりサイズも可能
/// </summary>
/// <param name="readImageFileName">画像ファイルの名前(パス)</param>
/// <param name="saveImageFileName">画像ファイルの保存名(パス)</param>
/// <param name="x">切り抜き矩形部分左上の基準点X座標</param>
/// <param name="y">切り抜き矩形部分左上の基準点Y座標</param>
/// <param name="width">切り抜き矩形部分の幅</param>
/// <param name="height">切り抜き矩形部分の高さ</param>
/// <param name="outputWidth">吐き出し画像サイズの幅</param>
/// <param name="outputHeight">吐き出し画像サイズの高さ</param>
void OpenCVLibrary::ImageProcesser::saveCutDarkImage(System::String ^readImageFileName, System::String ^saveImageFileName, int x, int y, int width, int height, int outputWidth, int outputHeight) {
	std::string stringReadImageFileName = msclr::interop::marshal_as<std::string>(readImageFileName);
	std::string stringSaveImageFileName = msclr::interop::marshal_as<std::string>(saveImageFileName);
	cv::Mat hsvImage;
	
	//画像読み込み
	cv::Mat originalImage = cv::imread(stringReadImageFileName);

	cv::Rect rect(x, y, width, height);
	cv::Mat result = cv::Mat(originalImage, rect).clone();

	//HSV変換
	cv::cvtColor(result, hsvImage, cv::COLOR_BGR2HSV);

	//V値を半分に
	for (int y = 0; y < hsvImage.rows; y++) {
		for (int x = 0; x < hsvImage.cols; x++) {
			hsvImage.at<cv::Vec3b>(y, x)[2] *= 0.8;
		}
	}

	//BGR変換
	cv::cvtColor(hsvImage, result, cv::COLOR_HSV2BGR);

	//resize
	cv::resize(result, result, cv::Size(), (double)outputWidth / (double)result.cols, (double)outputHeight / (double)result.rows);

	cv::imwrite(stringSaveImageFileName, result);

}



void OpenCVLibrary::ImageProcesser::waterSurfaceDraw(System::String ^readFileName, System::String ^saveFileName, double waterSurfaceRatio) {
	std::string stringReadImageFileName = msclr::interop::marshal_as<std::string>(readFileName);
	std::string stringSaveImageFileName = msclr::interop::marshal_as<std::string>(saveFileName);
	cv::Mat hsvImage;

	//画像読み込み
	cv::Mat image = cv::imread(stringReadImageFileName);

	int waterSurface = static_cast<int>(image.size().height * waterSurfaceRatio);


	//V値を半分に
	for (int x = 0; x < image.cols; x++) {
		image.at<cv::Vec3b>(image.size().height - waterSurface, x)[0] = 0;
		image.at<cv::Vec3b>(image.size().height - waterSurface, x)[1] = 0;
		image.at<cv::Vec3b>(image.size().height - waterSurface, x)[2] = 255;
	}

	cv::imwrite(stringSaveImageFileName, image);
}