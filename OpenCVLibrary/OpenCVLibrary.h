// OpenCVLibrary.h

#pragma once
#include <opencv2/opencv.hpp>
#include <string>
#include <msclr/marshal.h>
#include <msclr/marshal_cppstd.h>

using namespace System;

namespace OpenCVLibrary {
	/// <summary>
	/// 画像を加工する関数を集めたクラス
	/// </summary>
	public ref class ImageProcesser {
	public:
		static void showOriginalAndGrayScaleImage();
		static void showGrayScaleImage(System::String ^filename);
		static void saveCutImage(System::String ^readImageFileName, System::String ^saveImageFileName, int x, int y, int width, int height, int outputWidth, int outputHeight);
		static void saveCutGrayScaleImage(System::String ^readImageFileName, System::String ^saveImageFileName, int x, int y, int width, int height, int outputWidth, int outputHeight);
		static void saveCutDarkImage(System::String ^readImageFileName, System::String ^saveImageFileName, int x, int y, int width, int height, int outputWidth, int outputHeight);
		static void OpenCVLibrary::ImageProcesser::waterSurfaceDraw(System::String ^readFileName, System::String ^saveFileName, double waterSurfaceRatio);
	};
}
