﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using OpenCVLibrary;

namespace CutImageApplication {
    public partial class ControllForm : Form {
        private ImageForm imageForm = null;

        public ControllForm() {
            InitializeComponent();
            this.OutputTypeComboBox.SelectedIndex = 0;
            this.OutputModeComboBox.SelectedIndex = 0;

            //切り抜き範囲テキストボックス初期化
            controlPointXTextBox.Text = "400";
            controlPointYTextBox.Text = "200";
            cutAreaWidthTextBox.Text = "100";
            cutAreaHeightTextBox.Text = "100";
            
            //出力サイズ初期化
            outputImageWidthTextBox.Text = "28";
            outputImageHeightTextBox.Text = "28";
        }

        private void selectFileButton_MouseClick(object sender, MouseEventArgs e) {

            //現状jpgファイルにだけ対応
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "jpeg画像(*.jpg)|*.jpg|png画像(*.png)|*.png";
            ofd.InitialDirectory = System.IO.Directory.GetCurrentDirectory();

            if (ofd.ShowDialog() ==  DialogResult.OK) {
                //テキストボックスに選択された画像の名前を表示
                this.readImageFileNameTextBox.Text = ofd.FileName;
                
                //既に画像を選択して開いている場合はフォームを閉じる
                if(this.imageForm != null) {
                    imageForm.Close();
                    imageForm = null;
                }

                //選択画像読み込み
                System.Drawing.Image img = System.Drawing.Image.FromFile(ofd.FileName);

                //画像の幅と高さを表示
                this.readImageWidthTextBox.Text = img.Width.ToString();
                this.readImageHeightTextBox.Text = img.Height.ToString();

                ////切り抜き範囲テキストボックス初期化
                //controlPointXTextBox.Text = "400";
                //controlPointYTextBox.Text = "200";
                //cutAreaWidthTextBox.Text = "100";
                //cutAreaHeightTextBox.Text = "100";

                //別フォームに画像を表示
                this.imageForm = new ImageForm(this,
                                               img, 
                                               Int32.Parse(controlPointXTextBox.Text),
                                               Int32.Parse(controlPointYTextBox.Text),
                                               Int32.Parse(cutAreaWidthTextBox.Text),
                                               Int32.Parse(cutAreaHeightTextBox.Text));
                imageForm.Show();
            }

        }

        /// <summary>
        /// 切り抜き範囲指定部分のテキストボックスには数字しか入力できないように
        /// KeyPressEventArgsはこの関数を通す
        /// </summary>
        /// <param name="e">Keyが押されたときのイベントを受け取る</param>
        private void controlTextBox_KeyPressChecker(KeyPressEventArgs e) {
            if ((e.KeyChar < '0' || '9' < e.KeyChar) && e.KeyChar != '\b') {
                e.Handled = true;
            }
        }
        
        private void controlPointXTextBox_KeyPress(object sender, KeyPressEventArgs e) {
            controlTextBox_KeyPressChecker(e);
        }

        private void controlPointYTextBox_KeyPress(object sender, KeyPressEventArgs e) {
            controlTextBox_KeyPressChecker(e);
        }

        private void cutAreaWidthTextBox_KeyPress(object sender, KeyPressEventArgs e) {
            controlTextBox_KeyPressChecker(e);
        }

        private void cutAreaTextBox_KeyPress(object sender, KeyPressEventArgs e) {
            controlTextBox_KeyPressChecker(e);
        }

        private void outputImageWidthTextBox_KeyPress(object sender, KeyPressEventArgs e) {
            controlTextBox_KeyPressChecker(e);
        }

        private void outputImageHeightTextBox_KeyPress(object sender, KeyPressEventArgs e) {
            controlTextBox_KeyPressChecker(e);
        }

        private void outputNumTextBox_KeyPress(object sender, KeyPressEventArgs e) {
            controlTextBox_KeyPressChecker(e);
        }

        /// <summary>
        /// 基準点Xを決めるテキストボックスから
        /// フォーカスが外れた時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void controlPointXTextBox_Leave(object sender, EventArgs e) {
            int x = Convert.ToInt32(controlPointXTextBox.Text);
            int readImageWidth = Convert.ToInt32(readImageWidthTextBox.Text);
            int cutAreaWidth = Convert.ToInt32(cutAreaWidthTextBox.Text);

            //基準点Xはマイナスを許さない
            if(x < 0) {
                x = 0;
            }

            //基準点Xは画像の横幅を超えることを許さない
            if(readImageWidth <= x) {
                x = readImageWidth - 1;
            }

            //基準点がずらされたことによる切り取り範囲の調整
            if(readImageWidth < x + cutAreaWidth) {
                cutAreaWidth = readImageWidth - x;
            }

            imageForm.controlPointX = x;
            imageForm.cutAreaWidth = cutAreaWidth;
            controlPointXTextBox.Text = x.ToString();
            cutAreaWidthTextBox.Text = cutAreaWidth.ToString();

            imageForm.drawMarker();
        }

        /// <summary>
        /// 基準点Yを決めるテキストボックスから
        /// フォーカスが外れた時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void controlPointYTextBox_Leave(object sender, EventArgs e) {
            int y = Convert.ToInt32(controlPointYTextBox.Text);
            int readImageHeight = Convert.ToInt32(readImageHeightTextBox.Text);
            int cutAreaHeight = Convert.ToInt32(cutAreaHeightTextBox.Text);

            //基準点Yはマイナスを許さない
            if (y < 0) {
                y = 0;
            }

            //基準点Yは画像の縦幅を超えることを許さない
            if (readImageHeight <= y) {
                y = readImageHeight - 1;
            }

            //基準点がずらされたことによる切り取り範囲の調整
            if (readImageHeight < y + cutAreaHeight) {
                cutAreaHeight = readImageHeight - y;
            }

            imageForm.controlPointY = y;
            imageForm.cutAreaHeight = cutAreaHeight;
            controlPointYTextBox.Text = y.ToString();
            cutAreaHeightTextBox.Text = cutAreaHeight.ToString();

            imageForm.drawMarker();
        }

        /// <summary>
        /// 切り取り範囲の横幅を決めるテキストボックスから
        /// フォーカスが外れた時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cutWidthTextBox_Leave(object sender, EventArgs e) {
            int x = Convert.ToInt32(controlPointXTextBox.Text);
            int readImageWidth = Convert.ToInt32(readImageWidthTextBox.Text);
            int cutAreaWidth = Convert.ToInt32(cutAreaWidthTextBox.Text);

            //基準点がずらされたことによる切り取り範囲の調整
            if (readImageWidth < x + cutAreaWidth) {
                cutAreaWidth = readImageWidth - x;
            }

            imageForm.cutAreaWidth = cutAreaWidth;
            cutAreaWidthTextBox.Text = cutAreaWidth.ToString();

            imageForm.drawMarker();
        }

        /// <summary>
        /// 切り取り範囲の縦幅を決めるテキストボックスから
        /// フォーカスが外れた時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cutHeightTextBox_Leave(object sender, EventArgs e) {
            int y = Convert.ToInt32(controlPointYTextBox.Text);
            int readImageHeight = Convert.ToInt32(readImageHeightTextBox.Text);
            int cutAreaHeight = Convert.ToInt32(cutAreaHeightTextBox.Text);

            //基準点がずらされたことによる切り取り範囲の調整
            if (readImageHeight < y + cutAreaHeight) {
                cutAreaHeight = readImageHeight - y;
            }

            imageForm.cutAreaHeight = cutAreaHeight;
            cutAreaHeightTextBox.Text = cutAreaHeight.ToString();

            imageForm.drawMarker();
        }

        /// <summary>
        /// 画像の吐き出しサイズを決めるテキストボックスからフォーカスが外れた時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void outputImageWidthTextBox_Leave(object sender, EventArgs e) {
            int originalWidth = Convert.ToInt32(readImageWidthTextBox.Text.ToString());
            int outputImageWidth = Convert.ToInt32(outputImageWidthTextBox.Text.ToString());

            if(originalWidth < outputImageWidth) {
                outputImageWidthTextBox.Text = originalWidth.ToString();
            }
        }

        /// <summary>
        /// 画像の吐き出しサイズを決めるテキストボックスからフォーカスが外れた時の処理
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void outputImageHeightTextBox_Leave(object sender, EventArgs e) {
            int originalHeight = Convert.ToInt32(readImageHeightTextBox.Text.ToString());
            int outputImageHeight = Convert.ToInt32(outputImageHeightTextBox.Text.ToString());

            if (originalHeight < outputImageHeight) {
                outputImageHeightTextBox.Text = originalHeight.ToString();
            }
        }

        /// <summary>
        /// 保存ボタンが押された時の挙動
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveButton_MouseClick(object sender, MouseEventArgs e) {

            //画像が選択されているかチェック
            if (readImageFileNameTextBox.Text == "") {
                MessageBox.Show("ファイルを選択してください");
                return;
            }

            //水面位置チェック
            if (!(imageForm.controlPointY <= imageForm.waterSurface &&
                  imageForm.waterSurface <= imageForm.controlPointY + imageForm.cutAreaHeight)) {
                MessageBox.Show("水面は矩形範囲に収まるように指定してください");
                return;
            }




            // ランダム保存モードと一括保存モード
            String saveDirectoryName = "";
            String extension = System.IO.Path.GetExtension(readImageFileNameTextBox.Text);
            switch (OutputModeComboBox.SelectedItem.ToString()) {
                case "ランダム出力":
                    //保存先フォルダの設定と生成
                    saveDirectoryName = System.IO.Directory.GetParent(readImageFileNameTextBox.Text).ToString() + "\\" +
                                        System.IO.Path.GetFileNameWithoutExtension(readImageFileNameTextBox.Text).ToString() + "\\";
                    String saveFileNamePrefix = System.IO.Path.GetFileNameWithoutExtension(readImageFileNameTextBox.Text).ToString() + "_";
                    if (!System.IO.Directory.Exists(saveDirectoryName)) {
                        System.IO.Directory.CreateDirectory(saveDirectoryName);
                    }
                    saveRandomCutImageAndCsv(saveDirectoryName, saveFileNamePrefix, extension);
                    break;
                case "全画像一括処理":
                    //保存先フォルダの設定と生成
                    saveDirectoryName = System.IO.Directory.GetParent(readImageFileNameTextBox.Text).ToString() + "\\saveDir\\";
                    String imagesDir = System.IO.Directory.GetParent(readImageFileNameTextBox.Text).ToString();
                    if (!System.IO.Directory.Exists(saveDirectoryName)) {
                        System.IO.Directory.CreateDirectory(saveDirectoryName);
                    }
                    saveAllCutImages(saveDirectoryName, imagesDir, extension);
                    break;
                default:
                    break;
            }

            MessageBox.Show("出力完了", "完了",MessageBoxButtons.OK);
        }

        private void saveRandomCutImageAndCsv(String saveDirectoryName, String saveFileNamePrefix, String extension) {
            List<SaveInfo> saveInfoList = new List<SaveInfo>();

            Console.WriteLine(imageForm.imageHeight + ":" + imageForm.cutAreaHeight);
            int movingMax = getMovingMax(imageForm.controlPointY, imageForm.imageHeight, imageForm.cutAreaHeight);
            int movingMin = getMovingMin(imageForm.controlPointY, imageForm.imageHeight, imageForm.cutAreaHeight);
            int outputImageNum = Convert.ToInt32(outputNumTextBox.Text);
            Console.WriteLine(imageForm.waterSurface);
            Random random = new System.Random();
            for (int i = 0; i < outputImageNum; i++) {
                int movingY = random.Next(movingMin, movingMax);
                //画像保存
                saveCutImage(readImageFileNameTextBox.Text.ToString(),
                             saveDirectoryName + saveFileNamePrefix + i.ToString() + extension,
                             OutputTypeComboBox.SelectedItem.ToString(),
                             imageForm.controlPointX,
                             movingY,
                             imageForm.cutAreaWidth,
                             imageForm.cutAreaHeight);

                //リストにファイル名と水面位置の割合を格納
                saveInfoList.Add(new SaveInfo(saveFileNamePrefix + i.ToString() + extension,
                                              getWaterSurfaceRatio(movingY, imageForm.cutAreaHeight, imageForm.waterSurface)));
            }

            //CSV保存
            generateResultCsvFile(saveInfoList,
                                  saveDirectoryName +
                                  System.IO.Path.GetFileNameWithoutExtension(readImageFileNameTextBox.Text).ToString() +
                                  ".csv");
        }

        /// <summary>
        /// 指定ディレクトリ以下の
        /// 画像を全て切り取った画像を用意する関数
        /// </summary>
        /// <param name="saveDirectoryName"></param>
        private void saveAllCutImages(String saveDirectoryName, String imagesDir, String extension) {
            List<SaveInfo> saveInfoList = new List<SaveInfo>();
            Console.WriteLine(imageForm.imageHeight + ":" + imageForm.cutAreaHeight);

            string[] files = System.IO.Directory.GetFiles(imagesDir, "*" + extension, System.IO.SearchOption.TopDirectoryOnly);

            for (int i = 0; i < files.Length; i++) {
                Console.WriteLine(files[i]);
            }

            

            for (int i = 0; i < files.Length; i++) {
                //画像保存
                saveCutImage(files[i],
                             saveDirectoryName + System.IO.Path.GetFileName(files[i]),
                             OutputTypeComboBox.SelectedItem.ToString(),
                             imageForm.controlPointX,
                             imageForm.controlPointY,
                             imageForm.cutAreaWidth,
                             imageForm.cutAreaHeight);
                //リストにファイル名と水面位置の割合を格納
                saveInfoList.Add(new SaveInfo(System.IO.Path.GetFileName(files[i]),
                                              getWaterSurfaceRatio(Convert.ToInt32(controlPointYTextBox.Text.ToString()), imageForm.cutAreaHeight, imageForm.waterSurface)));
            }
            //CSV保存
            generateResultCsvFile(saveInfoList, saveDirectoryName + "filelist.csv");
        }



        /// <summary>
        /// 現在の矩形範囲の基準点Y座標を
        /// 下にどれだけずらせるかを返す
        /// </summary>
        /// <returns></returns>
        private int getMovingMax(int y, int imageHeight, int cutAreaHeight) {
            //if (imageHeight < imageForm.waterSurface + cutAreaHeight) {
            //    return imageHeight - cutAreaHeight;
            //}

            if(imageHeight < imageForm.waterSurface - 5 + cutAreaHeight) {
                return imageHeight - cutAreaHeight;
            }

            //未実装
            Console.WriteLine(imageForm.waterSurface - 5 + cutAreaHeight);
            return imageForm.waterSurface - 5;
        }

        /// <summary>
        /// 現在の矩形範囲の基準点Y座標を
        /// 上にどれだけずらせるかを返す
        /// </summary>
        /// <returns></returns>
        private int getMovingMin(int y, int imageHeight, int cutAreaHeight) {
            //水面位置+5px分までは上にずらす。画像からはみ出るときは0設定。
            if(imageForm.waterSurface - (cutAreaHeight - 5) < 0) {
                return 0;
            }

            Console.WriteLine(imageForm.waterSurface - (cutAreaHeight - 5));
            return imageForm.waterSurface - (cutAreaHeight - 5);
        }

        /// <summary>
        /// 切り取り範囲の下部分を0
        /// 上部分を1とした時の水面の位置を
        /// 小数点第2位までの値として返す
        /// </summary>
        /// <returns></returns>
        private double getWaterSurfaceRatio(int y, int height, int waterSurface) {
            double result = (double)(y + height - waterSurface) / (double)height;
            return Math.Round(result, 2);
        }




        /// <summary>
        /// 切り取り位置をずらす際に
        /// 水面位置がはみ出ないか
        /// 元々のPictureBoxからはみ出さないかをチェックする
        /// </summary>
        /// <param name="y">切り取り範囲の基準座標のy値 ずらした後の値を入れる</param>
        /// <param name="height">切り取り範囲の縦幅</param>
        /// <returns></returns>
        private bool isProperMoving(int y, int height) {
            return y < imageForm.waterSurface &&
                   imageForm.waterSurface < y + height &&
                   0 < y &&
                   y + height < imageForm.imageHeight;
        }
        
        /// <summary>
        /// 切り取った画像を保存する関数
        /// </summary>
        /// <param name="saveImageFileName"></param>
        /// <param name="outputType"></param>
        /// <param name="x">切り取り範囲の基準になる座標</param>
        /// <param name="y">切り取り範囲の基準になる座標</param>
        /// <param name="width">切り取り範囲の横幅</param>
        /// <param name="height">切り取り範囲の縦幅</param>
        public void saveCutImage(string readImageFileName,string saveImageFileName, string outputType, int x, int y, int width, int height) {
            int outputImageWidth = Convert.ToInt32(outputImageWidthTextBox.Text.ToString());
            int outputImageHeight = Convert.ToInt32(outputImageHeightTextBox.Text.ToString());

            //ComboBoxの値によって出力される画像が異なる
            switch (outputType) {
                case "Color":
                    OpenCVLibrary.ImageProcesser.saveCutImage(readImageFileName,
                                                              saveImageFileName,
                                                              x,
                                                              y,
                                                              width,
                                                              height,
                                                              outputImageWidth,
                                                              outputImageHeight);
                    break;

                case "GrayScale":
                    OpenCVLibrary.ImageProcesser.saveCutGrayScaleImage(readImageFileName,
                                                                       saveImageFileName,
                                                                       x,
                                                                       y,
                                                                       width,
                                                                       height,
                                                                       outputImageWidth,
                                                                       outputImageHeight);
                    break;

                case "Dark":
                    OpenCVLibrary.ImageProcesser.saveCutDarkImage(readImageFileName,
                                                                  saveImageFileName,
                                                                  x,
                                                                  y,
                                                                  width,
                                                                  height,
                                                                  outputImageWidth,
                                                                  outputImageHeight);
                    break;
            }
            
        }

        /// <summary>
        /// 保存ファイル名とその水面位置の割合を格納したリストから
        /// CSVファイルを作る処理
        /// </summary>
        /// <param name="saveInfoList"></param>
        /// <param name="csvFileName"></param>
        public void generateResultCsvFile(List<SaveInfo> saveInfoList, string csvFileName) {
            System.IO.StreamWriter sw = new System.IO.StreamWriter(csvFileName, false);

            for (int i = 0; i < saveInfoList.Count(); i++){
                sw.WriteLine(saveInfoList[i].saveFileName + "," + Math.Round(saveInfoList[i].waterSurfaceRatio*100).ToString());
            }

            sw.Close();
        }

        public class SaveInfo {
            public string saveFileName;
            public double waterSurfaceRatio;

            public SaveInfo(string saveFileName, double waterSurfaceRatio) {
                this.saveFileName = saveFileName;
                this.waterSurfaceRatio = waterSurfaceRatio;
            }
        }

    }
}
