﻿namespace CutImageApplication
{
    partial class ControllForm
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.readImageFileNameTextBox = new System.Windows.Forms.TextBox();
            this.OutputTypeComboBox = new System.Windows.Forms.ComboBox();
            this.readImageLabel = new System.Windows.Forms.Label();
            this.readImageWidthLabel = new System.Windows.Forms.Label();
            this.readImageHeightLabel = new System.Windows.Forms.Label();
            this.readImageWidthTextBox = new System.Windows.Forms.TextBox();
            this.readImageHeightTextBox = new System.Windows.Forms.TextBox();
            this.cutAreaLabel = new System.Windows.Forms.Label();
            this.controlPointXLabel = new System.Windows.Forms.Label();
            this.controlPointYLabel = new System.Windows.Forms.Label();
            this.controlPointXTextBox = new System.Windows.Forms.TextBox();
            this.controlPointYTextBox = new System.Windows.Forms.TextBox();
            this.cutHeightLabel = new System.Windows.Forms.Label();
            this.cutWidthLabel = new System.Windows.Forms.Label();
            this.cutAreaWidthTextBox = new System.Windows.Forms.TextBox();
            this.cutAreaHeightTextBox = new System.Windows.Forms.TextBox();
            this.saveButton = new System.Windows.Forms.Button();
            this.saveSettingLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.outputImageSizeLabel = new System.Windows.Forms.Label();
            this.outputImageWidthTextBox = new System.Windows.Forms.TextBox();
            this.outputImageWidthLabel = new System.Windows.Forms.Label();
            this.outputImageHeightTextBox = new System.Windows.Forms.TextBox();
            this.outputImageHeightLabel = new System.Windows.Forms.Label();
            this.outputNumLabel = new System.Windows.Forms.Label();
            this.outputNumTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.OutputModeComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(8, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(69, 19);
            this.button2.TabIndex = 3;
            this.button2.Text = "画像選択";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.selectFileButton_MouseClick);
            // 
            // readImageFileNameTextBox
            // 
            this.readImageFileNameTextBox.Location = new System.Drawing.Point(83, 12);
            this.readImageFileNameTextBox.Name = "readImageFileNameTextBox";
            this.readImageFileNameTextBox.ReadOnly = true;
            this.readImageFileNameTextBox.Size = new System.Drawing.Size(389, 19);
            this.readImageFileNameTextBox.TabIndex = 7;
            // 
            // OutputTypeComboBox
            // 
            this.OutputTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OutputTypeComboBox.FormattingEnabled = true;
            this.OutputTypeComboBox.Items.AddRange(new object[] {
            "Color",
            "GrayScale",
            "Dark"});
            this.OutputTypeComboBox.Location = new System.Drawing.Point(104, 360);
            this.OutputTypeComboBox.Name = "OutputTypeComboBox";
            this.OutputTypeComboBox.Size = new System.Drawing.Size(121, 20);
            this.OutputTypeComboBox.TabIndex = 8;
            // 
            // readImageLabel
            // 
            this.readImageLabel.AutoSize = true;
            this.readImageLabel.Location = new System.Drawing.Point(17, 51);
            this.readImageLabel.Name = "readImageLabel";
            this.readImageLabel.Size = new System.Drawing.Size(82, 12);
            this.readImageLabel.TabIndex = 9;
            this.readImageLabel.Text = "選択画像サイズ";
            // 
            // readImageWidthLabel
            // 
            this.readImageWidthLabel.AutoSize = true;
            this.readImageWidthLabel.Location = new System.Drawing.Point(147, 51);
            this.readImageWidthLabel.Name = "readImageWidthLabel";
            this.readImageWidthLabel.Size = new System.Drawing.Size(32, 12);
            this.readImageWidthLabel.TabIndex = 10;
            this.readImageWidthLabel.Text = "width";
            // 
            // readImageHeightLabel
            // 
            this.readImageHeightLabel.AutoSize = true;
            this.readImageHeightLabel.Location = new System.Drawing.Point(296, 51);
            this.readImageHeightLabel.Name = "readImageHeightLabel";
            this.readImageHeightLabel.Size = new System.Drawing.Size(36, 12);
            this.readImageHeightLabel.TabIndex = 11;
            this.readImageHeightLabel.Text = "height";
            // 
            // readImageWidthTextBox
            // 
            this.readImageWidthTextBox.Location = new System.Drawing.Point(185, 48);
            this.readImageWidthTextBox.Name = "readImageWidthTextBox";
            this.readImageWidthTextBox.ReadOnly = true;
            this.readImageWidthTextBox.Size = new System.Drawing.Size(74, 19);
            this.readImageWidthTextBox.TabIndex = 12;
            this.readImageWidthTextBox.Text = "0";
            // 
            // readImageHeightTextBox
            // 
            this.readImageHeightTextBox.Location = new System.Drawing.Point(338, 48);
            this.readImageHeightTextBox.Name = "readImageHeightTextBox";
            this.readImageHeightTextBox.ReadOnly = true;
            this.readImageHeightTextBox.Size = new System.Drawing.Size(74, 19);
            this.readImageHeightTextBox.TabIndex = 13;
            this.readImageHeightTextBox.Text = "0";
            // 
            // cutAreaLabel
            // 
            this.cutAreaLabel.AutoSize = true;
            this.cutAreaLabel.Location = new System.Drawing.Point(11, 98);
            this.cutAreaLabel.Name = "cutAreaLabel";
            this.cutAreaLabel.Size = new System.Drawing.Size(94, 12);
            this.cutAreaLabel.TabIndex = 14;
            this.cutAreaLabel.Text = "切り抜き範囲指定";
            // 
            // controlPointXLabel
            // 
            this.controlPointXLabel.AutoSize = true;
            this.controlPointXLabel.Location = new System.Drawing.Point(22, 128);
            this.controlPointXLabel.Name = "controlPointXLabel";
            this.controlPointXLabel.Size = new System.Drawing.Size(48, 12);
            this.controlPointXLabel.TabIndex = 15;
            this.controlPointXLabel.Text = "基準点X";
            // 
            // controlPointYLabel
            // 
            this.controlPointYLabel.AutoSize = true;
            this.controlPointYLabel.Location = new System.Drawing.Point(214, 128);
            this.controlPointYLabel.Name = "controlPointYLabel";
            this.controlPointYLabel.Size = new System.Drawing.Size(48, 12);
            this.controlPointYLabel.TabIndex = 16;
            this.controlPointYLabel.Text = "基準点Y";
            // 
            // controlPointXTextBox
            // 
            this.controlPointXTextBox.Location = new System.Drawing.Point(82, 125);
            this.controlPointXTextBox.Name = "controlPointXTextBox";
            this.controlPointXTextBox.Size = new System.Drawing.Size(74, 19);
            this.controlPointXTextBox.TabIndex = 17;
            this.controlPointXTextBox.Text = "0";
            this.controlPointXTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.controlPointXTextBox_KeyPress);
            this.controlPointXTextBox.Leave += new System.EventHandler(this.controlPointXTextBox_Leave);
            // 
            // controlPointYTextBox
            // 
            this.controlPointYTextBox.Location = new System.Drawing.Point(268, 125);
            this.controlPointYTextBox.Name = "controlPointYTextBox";
            this.controlPointYTextBox.Size = new System.Drawing.Size(74, 19);
            this.controlPointYTextBox.TabIndex = 18;
            this.controlPointYTextBox.Text = "0";
            this.controlPointYTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.controlPointYTextBox_KeyPress);
            this.controlPointYTextBox.Leave += new System.EventHandler(this.controlPointYTextBox_Leave);
            // 
            // cutHeightLabel
            // 
            this.cutHeightLabel.AutoSize = true;
            this.cutHeightLabel.Location = new System.Drawing.Point(226, 170);
            this.cutHeightLabel.Name = "cutHeightLabel";
            this.cutHeightLabel.Size = new System.Drawing.Size(36, 12);
            this.cutHeightLabel.TabIndex = 19;
            this.cutHeightLabel.Text = "height";
            // 
            // cutWidthLabel
            // 
            this.cutWidthLabel.AutoSize = true;
            this.cutWidthLabel.Location = new System.Drawing.Point(38, 170);
            this.cutWidthLabel.Name = "cutWidthLabel";
            this.cutWidthLabel.Size = new System.Drawing.Size(32, 12);
            this.cutWidthLabel.TabIndex = 20;
            this.cutWidthLabel.Text = "width";
            // 
            // cutAreaWidthTextBox
            // 
            this.cutAreaWidthTextBox.Location = new System.Drawing.Point(82, 167);
            this.cutAreaWidthTextBox.Name = "cutAreaWidthTextBox";
            this.cutAreaWidthTextBox.Size = new System.Drawing.Size(74, 19);
            this.cutAreaWidthTextBox.TabIndex = 21;
            this.cutAreaWidthTextBox.Text = "0";
            this.cutAreaWidthTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cutAreaWidthTextBox_KeyPress);
            this.cutAreaWidthTextBox.Leave += new System.EventHandler(this.cutWidthTextBox_Leave);
            // 
            // cutAreaHeightTextBox
            // 
            this.cutAreaHeightTextBox.Location = new System.Drawing.Point(268, 167);
            this.cutAreaHeightTextBox.Name = "cutAreaHeightTextBox";
            this.cutAreaHeightTextBox.Size = new System.Drawing.Size(74, 19);
            this.cutAreaHeightTextBox.TabIndex = 22;
            this.cutAreaHeightTextBox.Text = "0";
            this.cutAreaHeightTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cutAreaTextBox_KeyPress);
            this.cutAreaHeightTextBox.Leave += new System.EventHandler(this.cutHeightTextBox_Leave);
            // 
            // saveButton
            // 
            this.saveButton.Location = new System.Drawing.Point(397, 389);
            this.saveButton.Name = "saveButton";
            this.saveButton.Size = new System.Drawing.Size(75, 23);
            this.saveButton.TabIndex = 23;
            this.saveButton.Text = "保存";
            this.saveButton.UseVisualStyleBackColor = true;
            this.saveButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.saveButton_MouseClick);
            // 
            // saveSettingLabel
            // 
            this.saveSettingLabel.AutoSize = true;
            this.saveSettingLabel.Location = new System.Drawing.Point(11, 238);
            this.saveSettingLabel.Name = "saveSettingLabel";
            this.saveSettingLabel.Size = new System.Drawing.Size(53, 12);
            this.saveSettingLabel.TabIndex = 24;
            this.saveSettingLabel.Text = "保存設定";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 363);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 25;
            this.label2.Text = "画像処理";
            // 
            // outputImageSizeLabel
            // 
            this.outputImageSizeLabel.AutoSize = true;
            this.outputImageSizeLabel.Location = new System.Drawing.Point(12, 265);
            this.outputImageSizeLabel.Name = "outputImageSizeLabel";
            this.outputImageSizeLabel.Size = new System.Drawing.Size(58, 12);
            this.outputImageSizeLabel.TabIndex = 26;
            this.outputImageSizeLabel.Text = "出力サイズ";
            // 
            // outputImageWidthTextBox
            // 
            this.outputImageWidthTextBox.Location = new System.Drawing.Point(140, 262);
            this.outputImageWidthTextBox.Name = "outputImageWidthTextBox";
            this.outputImageWidthTextBox.Size = new System.Drawing.Size(74, 19);
            this.outputImageWidthTextBox.TabIndex = 27;
            this.outputImageWidthTextBox.Text = "0";
            this.outputImageWidthTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.outputImageWidthTextBox_KeyPress);
            this.outputImageWidthTextBox.Leave += new System.EventHandler(this.outputImageWidthTextBox_Leave);
            // 
            // outputImageWidthLabel
            // 
            this.outputImageWidthLabel.AutoSize = true;
            this.outputImageWidthLabel.Location = new System.Drawing.Point(102, 265);
            this.outputImageWidthLabel.Name = "outputImageWidthLabel";
            this.outputImageWidthLabel.Size = new System.Drawing.Size(32, 12);
            this.outputImageWidthLabel.TabIndex = 28;
            this.outputImageWidthLabel.Text = "width";
            // 
            // outputImageHeightTextBox
            // 
            this.outputImageHeightTextBox.Location = new System.Drawing.Point(310, 262);
            this.outputImageHeightTextBox.Name = "outputImageHeightTextBox";
            this.outputImageHeightTextBox.Size = new System.Drawing.Size(74, 19);
            this.outputImageHeightTextBox.TabIndex = 30;
            this.outputImageHeightTextBox.Text = "0";
            this.outputImageHeightTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.outputImageHeightTextBox_KeyPress);
            this.outputImageHeightTextBox.Leave += new System.EventHandler(this.outputImageHeightTextBox_Leave);
            // 
            // outputImageHeightLabel
            // 
            this.outputImageHeightLabel.AutoSize = true;
            this.outputImageHeightLabel.Location = new System.Drawing.Point(268, 265);
            this.outputImageHeightLabel.Name = "outputImageHeightLabel";
            this.outputImageHeightLabel.Size = new System.Drawing.Size(36, 12);
            this.outputImageHeightLabel.TabIndex = 29;
            this.outputImageHeightLabel.Text = "height";
            // 
            // outputNumLabel
            // 
            this.outputNumLabel.AutoSize = true;
            this.outputNumLabel.Location = new System.Drawing.Point(11, 308);
            this.outputNumLabel.Name = "outputNumLabel";
            this.outputNumLabel.Size = new System.Drawing.Size(53, 12);
            this.outputNumLabel.TabIndex = 31;
            this.outputNumLabel.Text = "出力枚数";
            // 
            // outputNumTextBox
            // 
            this.outputNumTextBox.Location = new System.Drawing.Point(82, 305);
            this.outputNumTextBox.Name = "outputNumTextBox";
            this.outputNumTextBox.Size = new System.Drawing.Size(74, 19);
            this.outputNumTextBox.TabIndex = 32;
            this.outputNumTextBox.Text = "100";
            this.outputNumTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.outputNumTextBox_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 394);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 34;
            this.label1.Text = "画像出力方式";
            // 
            // OutputModeComboBox
            // 
            this.OutputModeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.OutputModeComboBox.FormattingEnabled = true;
            this.OutputModeComboBox.Items.AddRange(new object[] {
            "ランダム出力",
            "全画像一括処理"});
            this.OutputModeComboBox.Location = new System.Drawing.Point(104, 391);
            this.OutputModeComboBox.Name = "OutputModeComboBox";
            this.OutputModeComboBox.Size = new System.Drawing.Size(121, 20);
            this.OutputModeComboBox.TabIndex = 33;
            // 
            // ControllForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 434);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.OutputModeComboBox);
            this.Controls.Add(this.outputNumTextBox);
            this.Controls.Add(this.outputNumLabel);
            this.Controls.Add(this.outputImageHeightTextBox);
            this.Controls.Add(this.outputImageHeightLabel);
            this.Controls.Add(this.outputImageWidthLabel);
            this.Controls.Add(this.outputImageWidthTextBox);
            this.Controls.Add(this.outputImageSizeLabel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.saveSettingLabel);
            this.Controls.Add(this.saveButton);
            this.Controls.Add(this.cutAreaHeightTextBox);
            this.Controls.Add(this.cutAreaWidthTextBox);
            this.Controls.Add(this.cutWidthLabel);
            this.Controls.Add(this.cutHeightLabel);
            this.Controls.Add(this.controlPointYTextBox);
            this.Controls.Add(this.controlPointXTextBox);
            this.Controls.Add(this.controlPointYLabel);
            this.Controls.Add(this.controlPointXLabel);
            this.Controls.Add(this.cutAreaLabel);
            this.Controls.Add(this.readImageHeightTextBox);
            this.Controls.Add(this.readImageWidthTextBox);
            this.Controls.Add(this.readImageHeightLabel);
            this.Controls.Add(this.readImageWidthLabel);
            this.Controls.Add(this.readImageLabel);
            this.Controls.Add(this.OutputTypeComboBox);
            this.Controls.Add(this.readImageFileNameTextBox);
            this.Controls.Add(this.button2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "ControllForm";
            this.Text = "正解画像作る君";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox readImageFileNameTextBox;
        private System.Windows.Forms.ComboBox OutputTypeComboBox;
        private System.Windows.Forms.Label readImageLabel;
        private System.Windows.Forms.Label readImageWidthLabel;
        private System.Windows.Forms.Label readImageHeightLabel;
        private System.Windows.Forms.TextBox readImageWidthTextBox;
        private System.Windows.Forms.TextBox readImageHeightTextBox;
        private System.Windows.Forms.Label cutAreaLabel;
        private System.Windows.Forms.Label controlPointXLabel;
        private System.Windows.Forms.Label controlPointYLabel;
        private System.Windows.Forms.TextBox controlPointXTextBox;
        private System.Windows.Forms.TextBox controlPointYTextBox;
        private System.Windows.Forms.Label cutHeightLabel;
        private System.Windows.Forms.Label cutWidthLabel;
        private System.Windows.Forms.TextBox cutAreaWidthTextBox;
        private System.Windows.Forms.TextBox cutAreaHeightTextBox;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Label saveSettingLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label outputImageSizeLabel;
        private System.Windows.Forms.TextBox outputImageWidthTextBox;
        private System.Windows.Forms.Label outputImageWidthLabel;
        private System.Windows.Forms.TextBox outputImageHeightTextBox;
        private System.Windows.Forms.Label outputImageHeightLabel;
        private System.Windows.Forms.Label outputNumLabel;
        private System.Windows.Forms.TextBox outputNumTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox OutputModeComboBox;
    }
}

