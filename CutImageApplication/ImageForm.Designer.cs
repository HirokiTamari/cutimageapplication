﻿namespace CutImageApplication {
    partial class ImageForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.basePictureBox = new System.Windows.Forms.PictureBox();
            this.coverPictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.basePictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.coverPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // basePictureBox
            // 
            this.basePictureBox.Location = new System.Drawing.Point(12, 12);
            this.basePictureBox.Name = "basePictureBox";
            this.basePictureBox.Size = new System.Drawing.Size(100, 50);
            this.basePictureBox.TabIndex = 0;
            this.basePictureBox.TabStop = false;
            // 
            // coverPictureBox
            // 
            this.coverPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.coverPictureBox.Location = new System.Drawing.Point(12, 12);
            this.coverPictureBox.Name = "coverPictureBox";
            this.coverPictureBox.Size = new System.Drawing.Size(100, 50);
            this.coverPictureBox.TabIndex = 1;
            this.coverPictureBox.TabStop = false;
            this.coverPictureBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.coverPictureBox_MouseClick);
            // 
            // ImageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.coverPictureBox);
            this.Controls.Add(this.basePictureBox);
            this.Name = "ImageForm";
            this.Text = "読み込み画像";
            ((System.ComponentModel.ISupportInitialize)(this.basePictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.coverPictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox basePictureBox;
        private System.Windows.Forms.PictureBox coverPictureBox;
    }
}