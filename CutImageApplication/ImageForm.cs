﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CutImageApplication {
    public partial class ImageForm : Form {
        private const int margin = 10;
        private ControllForm controllForm = null;
        public int controlPointX;
        public int controlPointY;
        public int cutAreaWidth;
        public int cutAreaHeight;
        public int waterSurface;
        public int imageWidth;
        public int imageHeight;

        /// <summary>
        /// コンストラクタ
        /// ImageFormをインスタンス化するControllFormの情報は
        /// 相互のフォームの値をいじるために受け取っておく。
        /// </summary>
        /// <param name="controllForm"></param>
        public ImageForm(ControllForm controllForm, Image img, int x, int y, int width, int height) {
            InitializeComponent();
            this.controllForm = controllForm;

            //PictureBox２枚重ね。
            //上に重ねたPictureBoxの親を設定する。
            coverPictureBox.Parent = basePictureBox;
            coverPictureBox.BackColor = Color.Transparent;

            //画像表示フォームのクライアント領域を設定。上下左右に余白を取っている。
            this.ClientSize = new Size(img.Width + 2 * margin, img.Height + 2 * margin);

            //画像を描画するPictureBoxの位置、サイズ設定。
            basePictureBox.Location = new Point(margin, margin);
            basePictureBox.Size = new Size(img.Width, img.Height);

            //画像を描画するPictureBoxの位置、サイズ設定。
            //coverPictureBoxの親はbasePictureBoxになるので、
            //Locationが(0,0)設定なのに注意
            coverPictureBox.Location = new Point(0, 0);
            coverPictureBox.Size = new Size(img.Width, img.Height);

            //画像の設定
            basePictureBox.Image = img;

            //切り抜き範囲数値設定
            controlPointX = x;
            controlPointY = y;
            cutAreaWidth = width;
            cutAreaHeight = height;
            waterSurface = img.Height / 2;

            imageWidth = img.Width;
            imageHeight = img.Height;

            drawMarker();
        }

        /// <summary>
        /// 画像をクリックした際にクリックした位置を
        /// waterSurfaceに設定し、切り取り位置の再描画をする
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void coverPictureBox_MouseClick(object sender, MouseEventArgs e) {
            waterSurface = e.Y;
            drawMarker();
        }

        /// <summary>
        /// クリックした場所と切り取りの矩形範囲を画面に描画する
        /// </summary>
        public void drawMarker() {
            Bitmap canvas = new Bitmap(coverPictureBox.Width, coverPictureBox.Height);
            Graphics g = Graphics.FromImage(canvas);
            Pen waterSurfacePen = new Pen(Color.Red, 1);
            Pen cutAreaPen = new Pen(Color.LightGreen, 1);

            //水面位置の描画
            g.DrawLine(waterSurfacePen, 0, waterSurface, coverPictureBox.Width, waterSurface);

            //切り取り場所の描画
            g.DrawRectangle(cutAreaPen, controlPointX, controlPointY, cutAreaWidth, cutAreaHeight);

            coverPictureBox.Image = canvas;
        }
    }
}
